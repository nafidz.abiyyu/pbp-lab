**1. Apakah perbedaan antara JSON dan XML?**

JSON merupakan format pertukaran data berbasis teks yang menggunakan tipe data teks dan angka untuk merepresentasikan objek. Format ini berdasarkan subset dari JavaScript dan bebas bahasa.

XML merupakan format data berbasis teks yang berasal dari SGML dan ditulis dengan cara yang sama diikuti oleh HTML.

JSON berorientasi pada data sedangkan XML berorientasi pada dokumen.

JSON tidak memiliki tag awal dan akhir dan sintaksisnya lebih ringan dikarenakan berorientasi pada data. Sedangkan XML membutuhkan lebih banyak karakter untuk mewakili data yang sama.

JSON mendukung tipe data teks dan angka termasuk integer dan string, serta data terstruktur direpresentasikan menggunakan array dan objek. Berbeda dengan JSON, XML tidak didukung oleh tipe array, tetapi mendukung banyak tipe data seperti angka, teks, gambar, grafir, dll.

**2. Apakah perbedaan antara HTML dan XML?**

Definisi

HTML merupakan bahasa komputer yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks.

XML merupakan bahasa dinamis yang digunakan untuk mengangkut data dan bukan untuk menampilkan data

Tujuan 

HTML dirancang untuk memfasilitasi transfer dokumen berbasis web atau bagaimana format dari tampilan data.

XML dirancang menekankan pada struktur dan konteksnya

Perbedaan secara umum

HTML case insensitive sedangkan XML Case sensitive

XML didukung oleh namsespace sedangkan HTML tidak didukung oleh namespaces

HTML memiliki tag tertabas sedangkan tag XML dapat dikembangkan