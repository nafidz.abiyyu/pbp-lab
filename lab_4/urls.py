from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    path('addnote', add_note, name='add_note'),
    path('notelist', note_list, name='note_list'),
]