from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    response = {}
    balik = NoteForm(request.POST or None)
    if(balik.is_valid()and request.method == 'POST'):
        balik.save()
        return HttpResponseRedirect('/lab-4')
    response['balik'] = balik
    return render(request,'lab4_form.html', response)


def note_list(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
