from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
@login_required (login_url = '/admin/login/')

# Create your views here.
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    response = {}
    balik = FriendForm(request.POST or None)
    if(balik.is_valid()and request.method == 'POST'):
        balik.save()
        return HttpResponseRedirect('/lab-3')
    response['balik'] = balik
    return render(request,'lab3_form.html', response)
